package si.blackblox;

import lombok.extern.slf4j.Slf4j;

import javax.smartcardio.*;
import javax.xml.bind.DatatypeConverter;
import java.util.List;

@Slf4j
public class SmartcardReader {
    private final Certificate cardCertificate = new Certificate();
    private final Certificate caCertificate = new Certificate();
    private Card card;
    private String challenge;
    private String cardAuthToken;


    public static void main(String[] args) throws CardException {
        SmartcardReader smartcard = new SmartcardReader();
        smartcard.connect();
        smartcard.readIccAlt();
//        smartcard.readIcc();
//        smartcard.readIc();
//        smartcard.readApplicationIdentification();
//        smartcard.readCardCertificate();
//        smartcard.readCACertificate();
//        smartcard.readControllerActivityData();
//        smartcard.internalAuthenticate();
//        smartcard.externalAuthenticate();
//        smartcard.readInternal();
        smartcard.disconnect();
    }

    protected void connect() throws CardException {
        // Show the list of all available card readers:
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = factory.terminals().list();
        for(CardTerminal ct:terminals){
            log.info(ct.getName());
        }
        // Use the first card reader:
        CardTerminal terminal = terminals.get(0);
        // Establish a connection with the card:
        this.card = terminal.connect("*");
        log.info("Connected to card: " + this.card);
    }

    protected void disconnect() throws CardException {
        this.card.disconnect(false);
        log.info("Card disconnected.");
    }

    protected String execute(String command) throws CardException {
        return execute(command, null, null);
    }

    protected String execute(String command, byte[] data) throws CardException {
        return execute(command, data, null);
    }

    protected String execute(String command, String ne) throws CardException {
        return execute(command, null, ne);
    }

    protected String execute(String command, byte[] data, String ne) throws CardException {
//        log.info("Executing command: " + command);
        CardChannel channel = card.getBasicChannel();
        String[] commandDefrag = command.split("(?<=\\G.{2})");
        int cla = hexInt(commandDefrag[0]);
        int ins = hexInt(commandDefrag[1]);
        int p1 = hexInt(commandDefrag[2]);
        int p2 = hexInt(commandDefrag[3]);

        CommandAPDU commandAPDU = null;
        if ((data == null || data.length == 0) && (ne == null)) {
            commandAPDU = new CommandAPDU(cla, ins, p1, p2);
        } else if ((data == null || data.length == 0) && (ne != null)) {
            commandAPDU = new CommandAPDU(cla, ins, p1, p2, hexInt(ne));
        } else if ((data != null && data.length > 0) && (ne == null)) {
            commandAPDU = new CommandAPDU(cla, ins, p1, p2, data);
        } else if ((data != null && data.length > 0) && (ne != null)) {
            commandAPDU = new CommandAPDU(cla, ins, p1, p2, data, hexInt(ne));
        }

        ResponseAPDU r = channel.transmit(commandAPDU);
        String hex = DatatypeConverter.printHexBinary(r.getBytes());
//        log.info("Command response: " + hex);
        return hex;
    }

    protected String executeAlt(String command) throws CardException {
//        log.info("Executing command: " + command);
        CardChannel channel = card.getBasicChannel();

        CommandAPDU commandAPDU = new CommandAPDU(DatatypeConverter.parseHexBinary(command));

        ResponseAPDU r = channel.transmit(commandAPDU);
        String hex = DatatypeConverter.printHexBinary(r.getBytes());
//        log.info("Command response: " + hex);
        return hex;
    }

    protected int hexInt(String str) {
        return Integer.parseInt(str, 16);
    }

    protected void select(String fileId) throws CardException {
        byte[] data = DatatypeConverter.parseHexBinary(fileId);
        execute(Command.SELECT_FILE_BY_ID.getCode(), data);
    }

    protected void selectByAID(String fileId) throws CardException {
        byte[] data = DatatypeConverter.parseHexBinary(fileId);
        execute(Command.SELECT_FILE_BY_AID.getCode(), data);
    }

    protected String read(String startLoc, String ne) throws CardException {
        String command = Command.READ_BINARY.getCode() + startLoc;
        return execute(command, ne);
    }

    protected String readSec(String startLoc, String ne) throws CardException {
        String command = Command.READ_BINARY.getCode() + startLoc;
        byte[] data = DatatypeConverter.parseHexBinary("9701" + ne + "8E04" + this.cardAuthToken.substring(0, 8) + "00");
        return execute(command, data);
    }

    protected void readCardCertificate() throws CardException {
        selectByAID(CompanyCard.TACHO_APPLICATION.getFileId());
        //select CA certificate file
        select(CompanyCard.Card_Certificate.getFileId());
        //read first 16 bytes
        String response = read("0000", "C2");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain Card Certificate");
        }

        //remove status words from response
        this.cardCertificate.setCert(response.substring(0, response.length() - 4));
        log.info("Card_Certificate file: "+response.substring(0, response.length() - 4));

    }

    protected void readCACertificate() throws CardException {
        selectByAID(CompanyCard.TACHO_APPLICATION.getFileId());
        //select CA certificate file
        select(CompanyCard.CA_Certificate.getFileId());
        //read first 16 bytes
        String response = read("0000", "C2");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain CA Certificate");
        }

        //remove status words from response
        this.caCertificate.setCert(response.substring(0, response.length() - 4));
        log.info("CA_Certificate file: "+response.substring(0, response.length() - 4));

    }

    protected void readIcc() throws CardException {
        //select CA certificate file
        select(CompanyCard.Icc.getFileId());
        //read first 16 bytes
        String response = read("0000", "19");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain Icc file");
        }

        //remove status words from response
        log.info("Icc file: "+response.substring(0, response.length() - 4));

    }

    protected void readIccAlt() throws CardException {
        //select CA certificate file
        select(CompanyCard.Icc.getFileId());
        executeAlt("00A4020C020002");
        //read first 16 bytes
        String response = executeAlt("00B0000019");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain Icc file");
        }

        //remove status words from response
        log.info("Icc file: "+response.substring(0, response.length() - 4));

    }

    protected void readIc() throws CardException {
        //select CA certificate file
        select(CompanyCard.Ic.getFileId());
        //read first 16 bytes
        String response = read("0000", "08");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain Ic file");
        }

        //remove status words from response
        log.info("Ic file: "+response.substring(0, response.length() - 4));

    }

    protected void readApplicationIdentification() throws CardException {
        selectByAID(CompanyCard.TACHO_APPLICATION.getFileId());
        //select CA certificate file
        select(CompanyCard.Application_Identification.getFileId());
        //read first 16 bytes
        String response = read("0000", "05");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain Application_Identification file");
        }

        //remove status words from response
        log.info("Application_Identification file: "+response.substring(0, response.length() - 4));

    }

    protected void readControllerActivityData() throws CardException {
        selectByAID(CompanyCard.TACHO_APPLICATION.getFileId());
        //select CA certificate file
        select(CompanyCard.Controller_Activity_Data.getFileId());

        int le = 0xFF;
        int offset = 0x0;
        boolean eof = false;
        String contents = "";
        do {
            String response = read(String.format("%04X", offset), String.format("%02X", le));

            String responseStatus = responseStatus(response);
            log.info("      response status:"+responseStatus);


            if ("9000".equals(responseStatus)) {
                contents += response.substring(0, response.length() - 4);
                offset += le;
                if (offset == 0x5ca3) {
                    le=0xCF;
                }
            } else if (responseStatus.startsWith("6C")) {
                le = hexInt(responseStatus.substring(2));
            } else if ("6B00".equals(responseStatus)) {
                eof = true;
            } else {
                log.info("      full response:"+response);
                log.warn("Controller activity data read ended unexpectedly with status: " + responseStatus);
                log.info(contents);
                throw new IllegalStateException("Unable to read Controller activity data");
            }
        } while (!eof);
        log.info("Controller activity data:");
        log.info(contents);
    }

    protected void readInternal() throws CardException {
        selectByAID(CompanyCard.TACHO_APPLICATION.getFileId());
        //select CA certificate file
        select(CompanyCard.Identification.getFileId());
        //read first 16 bytes
        String response = readSec("0000", "8B");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to read internal");
        }

        //remove status words from response
        //this.cardCertificate.setCert(response.substring(0, response.length() - 4));

    }

    //for some commands, response status words are appended at the end of the response string
    //response status consists of two status words SW1 and SW2, each composed of two characters
    protected String responseStatus(String response) {
        return response.substring(response.length() - 4);
    }

    protected void setSecurityEnvironment(String keyIdentifier) throws CardException {
        byte[] data = DatatypeConverter.parseHexBinary("8308" + keyIdentifier);
        execute(Command.MANAGE_SECURITY_ENVIRONMENT.getCode(), data);
    }

    protected void getChanllenge() throws CardException {
        String response = execute(Command.GET_CHALLENGE.getCode(), "08");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to obtain challenge string");
        }

        //remove status words from response
        this.challenge = response.substring(0, response.length() - 4);
    }

    protected void internalAuthenticate() throws CardException {
        setSecurityEnvironment(this.cardCertificate.getCar());
        verifyCertificate(this.cardCertificate.getCert());
        getChanllenge();

        byte[] data = DatatypeConverter.parseHexBinary(this.challenge + this.cardCertificate.getCar());

        String response = execute(Command.INTERNAL_AUTHENTICATE.getCode(), data, "80");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("Unable to perform internal authenticate");
        }

        //remove status words from response
        this.cardAuthToken = response.substring(0, response.length() - 4);
        log.info("Card Auth token: " + this.cardAuthToken);
    }

    protected void externalAuthenticate() throws CardException {
        getChanllenge();
        byte[] data = DatatypeConverter.parseHexBinary(this.cardAuthToken);

        String response = execute(Command.EXTERNAL_AUTHENTICATE.getCode(), data, "80");

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            switch (responseStatus) {
                case "6A88":
                    throw new IllegalStateException("no Public Key is present in the Security Environment AND/OR no Private Key is present in the Security Environment");
                case "6F00":
                    throw new IllegalStateException("the CHA of the currently set public key is not the concatenation of the Tachograph application AID and of a VU equipment Type");
                case "6688":
                    throw new IllegalStateException("the verification of the cryptogram is wrong");
                case "6985":
                    throw new IllegalStateException("the command is not immediately preceded with a GET CHALLENGE command");
                case "6400":
                    throw new IllegalStateException("the selected private key is considered corrupted");
                case "6581":
                    throw new IllegalStateException("the selected private key is considered corrupted");
            }
        }
    }

    protected void verifyCertificate(String cert) throws CardException {
        byte[] data = DatatypeConverter.parseHexBinary(cert);

        String response = execute(Command.PSO_VERIFY_CERTIFICATE.getCode(), data);

        String responseStatus = responseStatus(response);

        if (!"9000".equals(responseStatus)) {
            throw new IllegalStateException("failed to verify certificate " + cert);
        }

    }
}
