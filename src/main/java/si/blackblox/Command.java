package si.blackblox;

public enum Command {
    SELECT_FILE_BY_AID("00A4040C"),
    SELECT_FILE_BY_ID("00A4020C"),
    READ_BINARY("00B0"),
    MANAGE_SECURITY_ENVIRONMENT("0022C1B6"),
    INTERNAL_AUTHENTICATE("00880000"),
    EXTERNAL_AUTHENTICATE("00820000"),
    PSO_VERIFY_CERTIFICATE("002A00AE"),
    GET_CHALLENGE("00840000");
    private String code;

    private Command(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
