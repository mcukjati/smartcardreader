package si.blackblox;

import lombok.Data;

@Data
public class Certificate {
    private String cert;
    public String getSign(){
        if(this.cert!=null){
            return this.cert.substring(0,256);
        }
        return null;
    }
    public String getCn(){
        if(this.cert!=null){
            return this.cert.substring(256,372);
        }
        return null;

    }
    public String getCar(){

        if(this.cert!=null){
            return this.cert.substring(372,388);
        }
        return null;
    }
}
