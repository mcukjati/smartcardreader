package si.blackblox;

public enum CompanyCard {
    Icc("0002"),
    Ic("0005"),
    TACHO_APPLICATION("FF544143484F"),
    Application_Identification("0501"),
    Card_Certificate("C108"),
    CA_Certificate("C100"),
    Identification("0520"),
    Controller_Activity_Data("050D");
    private String fileId;

    CompanyCard(String fileId) {
        this.fileId = fileId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
